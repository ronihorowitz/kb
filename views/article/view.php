<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Article */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Articles', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="article-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>


    <?php
        /*
        $models = $model->tags;
        $items = [];
        $tags = '';
        
        //prevent the first ','
        foreach ($models as $tag) {
            $tagLink = Html::a($tag->name, 
            ['article/index', 'ArticleSearch[tags]' => $tag->name]); 
            $tags .= ', '.$tagLink;
        } 
        
        $tags = substr($tags, 1); 
        */
    ?>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
            'descriptin',
            'body:ntext',
            'author_id',
            'editor_id',
            'category_id',
            [
                'label' => 'Tags', 
                'format' => 'html',
                'value' => $tags,
            ],          
            [                      
                'label' => 'Category',
                'value' => $model->category->name,
            ],            
            'created_at',
            'updated_at',
            'created_by',
            'updated_by',
        ],
    ]) ?>

</div>
